class FieldTrip < ActiveRecord::Base
  belongs_to :school
  has_many :registrations
  
  has_many :students, through: :registrations
end
