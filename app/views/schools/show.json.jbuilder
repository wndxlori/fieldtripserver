json.extract! @school, :id, :board, :category, :school_type, :name, :address, :city, :province, :postal_code, :grades, :ecs, :created_at, :updated_at
